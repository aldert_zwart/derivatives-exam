import numpy as np
from scipy.stats import norm
import math
import pandas as pd

def binomial(K,T,S0,r,N,u,d):
    # precompute constants
    dt = T/N
    q = (np.exp(r*dt)-d) / (u-d)
    disc = np.exp(-r*dt)
    # initialise asset prices at maturity - Time steps N
    S = np.zeros(N+1)
    S[0] = S0*d**N
    for j in range(1, N+1):
        S[j] = S[j-1]*u/d
    # initialise option values at maturity
    C = np.zeros(N+1)
    for j in range(0, N+1):
        C[j] = max(0, S[j]-K)

    # backwards discounting
    for i in np.arange(N,0,-1):
        for j in range(0,i):
            C[j] = disc * (q * C[j+1] + (1-q) * C[j])
    return C[0]




N_prime = norm.pdf
N = norm.cdf

def black_scholes_call(S, K, T, r, sigma):
    '''

    :param S: Asset price
    :param K: Strike price
    :param T: Time to maturity
    :param r: risk-free rate (treasury bills)
    :param sigma: volatility
    :return: call price
    '''

    ###standard black-scholes formula
    d1 = (np.log(S / K) + (r + sigma ** 2 / 2) * T) / (sigma * np.sqrt(T))
    d2 = d1 - sigma * np.sqrt(T)

    call = S * N(d1) - N(d2) * K * np.exp(-r * T)
    return call

def black_scholes_put(S, K, T, r, sigma):
    '''

    :param S: Asset price
    :param K: Strike price
    :param T: Time to maturity
    :param r: risk-free rate (treasury bills)
    :param sigma: volatility
    :return: call price
    '''

    ###standard black-scholes formula
    d1 = (np.log(S / K) + (r + sigma ** 2 / 2) * T) / (sigma * np.sqrt(T))
    d2 = d1 - sigma * np.sqrt(T)

    put = K * np.exp(-r * T) * N(-d2) - S * N(-d1)
    return put

N_prime = norm.pdf
N = norm.cdf
S = 70
K = 70*0.95
r = 0.041
T = 194/250
q = 0
sigma = 0.3
print(f"Calculated BS call price is: {black_scholes_call(S, K, T, r, sigma)}")
print(f"Calculated BS put price is: {black_scholes_put(S, K, T, r, sigma)}")


N = 100
u = 1.1
d = 1/u
S0 = 70
K = 70*0.95
r = 0.041
T = 194/250
print(f"Calculated Binomial call price is: {binomial(K,T,S0,r,N,u,d)}")

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm


def geo_paths(S, T, r, q, sigma, steps, N):
    """
    Inputs
    #S = Current stock Price
    #K = Strike Price
    #T = Time to maturity 1 year = 1, 1 months = 1/12
    #r = risk free interest rate
    #q = dividend yield
    # sigma = volatility

    Output
    # [steps,N] Matrix of asset paths
    """
    dt = T / steps
    # S_{T} = ln(S_{0})+\int_{0}^T(\mu-\frac{\sigma^2}{2})dt+\int_{0}^T \sigma dW(t)
    ST = np.log(S) + np.cumsum(((r - q - sigma ** 2 / 2) * dt + \
                                sigma * np.sqrt(dt) * \
                                np.random.normal(size=(steps, N))), axis=0)
    return np.exp(ST)

S = 70 #stock price S_{0}
# K (strike) is to simulate
T = 194/250 # time to maturity
r=0.041 # risk free risk in annual %
q = 0 # annual dividend rate
sigma = 0.3 # annual volatility in %
steps = 194 # time steps
N = 1000 # number of trials
t1=47
t2=98

paths= geo_paths(S,T,r, q,sigma,steps,N)

plt.plot(paths);
plt.xlabel("Time Increments")
plt.ylabel("Stock Price")
plt.title("Geometric Brownian Motion")
plt.show()

import pandas as pd
st1_pd = pd.DataFrame(paths[t1])
st1=st1_pd.mean()
print(f"MS sim {float(st1)}")